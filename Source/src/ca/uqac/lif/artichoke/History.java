/*
    Artichoke, enforcement of document lifecycles
    Copyright (C) 2016-2017 Sylvain Hallé
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.artichoke;

import java.util.LinkedList;

public class History extends LinkedList<HistoryElement>
{
	/**
	 * Dummy UID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates a new empty history
	 */
	public History()
	{
		super();
	}
	
	/**
	 * Creates a history by copying its contents from another
	 * @param h The other history
	 */
	public History(History h)
	{
		super();
		addAll(h);
	}
	
	/**
	 * Creates a history by copying its contents from another
	 * @param h The other history
	 * @param position The starting position from which to copy
	 */
	public History(History h, int position)
	{
		super();
		for (int i = position; i < h.size(); i++)
		{
			add(h.get(i));
		}
	}

}
