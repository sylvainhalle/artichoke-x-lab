package ca.uqac.lif.artichoke.lab;
/*
    Artichoke, enforcement of document lifecycles
    Copyright (C) 2016-2017 Sylvain Hallé
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import ca.uqac.lif.artichoke.Action;
import ca.uqac.lif.artichoke.HistoryManager;
import ca.uqac.lif.artichoke.Peer;
import ca.uqac.lif.artichoke.RsaFactory;
import ca.uqac.lif.labpal.FileHelper;
import ca.uqac.lif.labpal.Group;
import ca.uqac.lif.labpal.Laboratory;
import ca.uqac.lif.mtnp.plot.TwoDimensionalPlot.Axis;
import ca.uqac.lif.mtnp.plot.gnuplot.Scatterplot;
import ca.uqac.lif.mtnp.table.Join;
import ca.uqac.lif.mtnp.table.RenameColumns;
import ca.uqac.lif.mtnp.table.TransformedTable;
import ca.uqac.lif.labpal.table.ExperimentTable;

public class HistoryLab extends Laboratory
{
	/**
	 * The increment step for the length of the peer action sequences
	 */
	public static final transient int s_incrementStep = 50;
	
	/**
	 * The maximum length of the sequences
	 */
	public static final transient int s_maxLength = 2000;
	
	@Override
	public void setup()
	{
		setTitle("Decentralized enforcement of document lifecycle constraints");
		setDescription(FileHelper.internalFileToString(HistoryLab.class, "description.html"));
		setAuthor("Sylvain Hallé");
		HistoryManager manager = getHistoryManager();
		// Create experiment groups
		Group gen_group = new Group("Sequence generation");
		gen_group.setDescription("Experiments related to the generation of a peer-action sequence");
		add(gen_group);
		Group chk_group = new Group("Sequence verification");
		chk_group.setDescription("Experiments related to the verification of a peer-action sequence");
		add(chk_group);
		Group stateful_vs_stateless = new Group("Stateful vs. stateless verification");
		stateful_vs_stateless.setDescription("Experiments comparing the verification time between stateful and stateless peers");
		add(stateful_vs_stateless);
		// Create a table for generation time
		ExperimentTable runtime_tab = new ExperimentTable(GenerationExperiment.LENGTH, GenerationExperiment.TIME);
		runtime_tab.setTitle("Running time");
		add(runtime_tab);
		// Create a table for size consumed
		ExperimentTable space_tab = new ExperimentTable(GenerationExperiment.LENGTH, GenerationExperiment.SPACE);
		space_tab.setTitle("Space");
		add(space_tab);
		// Create a table for checking time
		ExperimentTable checking_tab = new ExperimentTable(GenerationExperiment.LENGTH, GenerationExperiment.TIME);
		checking_tab.setTitle("Verification time");
		add(checking_tab);
		// Create a table to compare stateful vs. stateless peers
		ExperimentTable stateful_peer_comparison = new ExperimentTable(GenerationExperiment.LENGTH, GenerationExperiment.TIME);
		stateful_peer_comparison.setTitle("Verification time for a stateful peer");
		ExperimentTable stateless_peer_comparison = new ExperimentTable(GenerationExperiment.LENGTH, GenerationExperiment.TIME);
		stateless_peer_comparison.setTitle("Verification time for a stateless peer");
		TransformedTable stateful_comparison = new TransformedTable(new Join(GenerationExperiment.LENGTH), 
				new TransformedTable(new RenameColumns(GenerationExperiment.LENGTH, "Stateful peer"), stateful_peer_comparison),
				new TransformedTable(new RenameColumns(GenerationExperiment.LENGTH, "Stateless peer"), stateless_peer_comparison));
		stateful_comparison.setTitle("Verification time for stateful vs. stateless peers");
		add(stateful_peer_comparison, stateless_peer_comparison, stateful_comparison);
		for (int length = s_incrementStep; length <= s_maxLength; length += s_incrementStep)
		{
			{
				GenerationExperiment ge = new GenerationExperiment(manager, length);
				add(ge);
				runtime_tab.add(ge);
				space_tab.add(ge);
				gen_group.add(ge);
			}
			{
				VerificationExperiment ve = new VerificationExperiment(manager, length);
				add(ve);
				checking_tab.add(ve);
				chk_group.add(ve);
				
			}
			{
				if (length % (2 * s_incrementStep) == 0 && length < s_maxLength / 2)
				{
					// We create fewer experiments of this type
					StatefulPeerExperiment sfpe = new StatefulPeerExperiment(manager, length);
					add(sfpe);
					stateful_peer_comparison.add(sfpe);
					stateful_vs_stateless.add(sfpe);
					StatelessPeerExperiment slpe = new StatelessPeerExperiment(manager, length);
					add(slpe);
					stateless_peer_comparison.add(slpe);
					stateful_vs_stateless.add(slpe);
				}
			}
		}
		// Create plots from these tables
		Scatterplot sp_gen = new Scatterplot(runtime_tab);
		sp_gen.setTitle("Generation time");
		add(sp_gen);
		Scatterplot sp_ver = new Scatterplot(checking_tab);
		sp_ver.setTitle("Verification time");
		add(sp_ver);
		Scatterplot sp_comp = new Scatterplot(stateful_comparison);
		sp_comp.setTitle("Verification time of stateful vs. stateless peers");
		sp_comp.setCaption(Axis.Y, "Time (ms)");
		sp_comp.setLogscale(Axis.Y);
		add(sp_comp);
	}
	

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		HistoryLab.initialize(args, HistoryLab.class);

	}

	protected static HistoryManager getHistoryManager()
	{
		RsaFactory factory = new RsaFactory();
		MessageDigest digest = null;
		try 
		{
			digest = MessageDigest.getInstance("MD5");
		} 
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
		HistoryManager hm = new HistoryManager(digest);
		Map<String,Peer> peers = new HashMap<String,Peer>();
		peers.put("Alice", factory.newPeer("Alice"));
		peers.put("Bob", factory.newPeer("Bob"));
		peers.put("Carl", factory.newPeer("Carl"));
		Map<String,ca.uqac.lif.artichoke.Group> groups = new HashMap<String,ca.uqac.lif.artichoke.Group>();
		groups.put("G1", factory.newGroup("G1"));
		groups.put("G2", factory.newGroup("G2"));
		Map<String,Action> actions = new HashMap<String,Action>();
		actions.put("a", factory.newAction("a"));
		actions.put("b", factory.newAction("b"));
		actions.put("c", factory.newAction("c"));
		hm.setPeerDirectory(peers);
		hm.setActionDirectory(actions);
		hm.setGroupDirectory(groups);
		return hm;
	}

}
