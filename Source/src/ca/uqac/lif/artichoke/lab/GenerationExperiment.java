package ca.uqac.lif.artichoke.lab;
/*
    Artichoke, enforcement of document lifecycles
    Copyright (C) 2016-2017 Sylvain Hallé
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import ca.uqac.lif.artichoke.EncryptionException;
import ca.uqac.lif.artichoke.History;
import ca.uqac.lif.artichoke.HistoryManager;
import ca.uqac.lif.labpal.Experiment;
import ca.uqac.lif.labpal.ExperimentException;

/**
 * Experiment measuring the time and space required to
 * generate a peer-action sequence.
 */
public class GenerationExperiment extends Experiment
{
	public static final transient String TIME = "Time";
	public static final transient String SPACE = "Space";
	public static final transient String LENGTH = "Length";
	
	/**
	 * The HistoryManager used to manipulated the history
	 */
	protected final transient HistoryManager m_manager;
	
	/**
	 * The number of times each experiment is repeated; we take the
	 * average of those values
	 */
	protected final static transient int s_numRepetitions = 3; 
	
	public GenerationExperiment(HistoryManager manager, int length)
	{
		super();
		setDescription("Experiment measuring the time and space required to generate a peer-action sequence.");
		describe(LENGTH, "The number of append operations performed");
		describe(TIME, "The number of milliseconds elapsed");
		describe(SPACE, "The size of the history");
		describe("Operation", "The type of operation done on the sequence");
		setInput("Operation", "Generation");
		setInput(LENGTH, length);
		m_manager = manager;
	}

	@Override
	public void execute() throws ExperimentException
	{
		int length = readInt("Length");
		long total_time = 0;
		for (int num_rep = 0; num_rep < s_numRepetitions; num_rep++)
		{
			long start_time = System.currentTimeMillis();
			History h = new History();
			for (int i = 0; i < length; i++)
			{
				try
				{
					m_manager.appendAction(h, "Alice", "a", "G1");
				} 
				catch (EncryptionException e) 
				{
					throw new ExperimentException(e);
				}
			}
			long end_time = System.currentTimeMillis();	
			total_time += (end_time - start_time);
			if (num_rep == 0)
			{
				// We only need to do this once
				String serialized = m_manager.serializeHistory(h);
				write(SPACE, serialized.length());
			}
		}
		// Since we are measuring milliseconds, we can afford
		// rounding to an integer
		int avg_time = (int) total_time / s_numRepetitions;
		write(TIME, avg_time);
	}

	@Override
	public float getDurationEstimate(float factor)
	{
		return readFloat(LENGTH) * 0.0025f / factor;
	}
}
