/*
    Artichoke, enforcement of document lifecycles
    Copyright (C) 2016-2017 Sylvain Hallé
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.artichoke.lab;

import ca.uqac.lif.artichoke.EncryptionException;
import ca.uqac.lif.artichoke.History;
import ca.uqac.lif.artichoke.HistoryManager;
import ca.uqac.lif.artichoke.InvalidHistoryException;
import ca.uqac.lif.labpal.ExperimentException;

public class StatefulPeerExperiment extends VerificationExperiment
{
	public StatefulPeerExperiment(HistoryManager manager, int length)
	{
		super(manager, length);
		setDescription("Experiment measuring the time required to verify an existing peer-action sequence using a stateful peer.");
		setInput("Operation", "Stateful Verification");
	}
	
	@Override
	public void execute() throws ExperimentException
	{
		int length = readInt("Length");
		History h = new History();
		long total_time = 0;
		// Generate a sequence of given length
		for (int i = 0; i < length; i++)
		{
			setProgression((float) i / (float) length);
			try
			{
				m_manager.appendAction(h, "Alice", "a", "G1");
			} 
			catch (EncryptionException e) 
			{
				throw new ExperimentException(e);
			}
			// For each new appended action, verify the sequence
			// In a stateful peer, the verification is incremental
			// from the next-to-last element
			if (i == 0)
				continue; // Except for the first element
			long start_time = System.currentTimeMillis();
			try 
			{
				m_manager.isHistoryValid(h, h.get(i - 1), i - 1);
			} 
			catch (InvalidHistoryException e) 
			{
				throw new ExperimentException(e);
			} 
			catch (EncryptionException e)
			{
				throw new ExperimentException(e);
			}
			long end_time = System.currentTimeMillis();
			total_time += (end_time - start_time);
		}
		// Since we are measuring milliseconds, we can afford
		// rounding to an integer
		int avg_time = (int) total_time / s_numRepetitions;
		write(TIME, avg_time);
	}
}
