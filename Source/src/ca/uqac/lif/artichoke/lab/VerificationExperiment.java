/*
    Artichoke, enforcement of document lifecycles
    Copyright (C) 2016-2017 Sylvain Hallé
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.artichoke.lab;

import ca.uqac.lif.artichoke.EncryptionException;
import ca.uqac.lif.artichoke.History;
import ca.uqac.lif.artichoke.HistoryManager;
import ca.uqac.lif.artichoke.InvalidHistoryException;
import ca.uqac.lif.labpal.Experiment;
import ca.uqac.lif.labpal.ExperimentException;

public class VerificationExperiment extends Experiment
{
	public static final transient String TIME = "Time";
	public static final transient String LENGTH = "Length";

	/**
	 * The HistoryManager used to manipulated the history
	 */
	protected final transient HistoryManager m_manager;

	/**
	 * The number of times each experiment is repeated; we take the
	 * average of those values
	 */
	protected final static transient int s_numRepetitions = 3; 

	public VerificationExperiment(HistoryManager manager, int length)
	{
		super();
		setDescription("Experiment measuring the time required to verify an existing peer-action sequence.");
		describe(LENGTH, "The length of the peer-action sequence");
		describe(TIME, "The number of milliseconds elapsed");
		describe("Operation", "The type of operation done on the sequence");
		setInput("Operation", "Verification");
		setInput(LENGTH, length);
		m_manager = manager;
	}

	@Override
	public void execute() throws ExperimentException
	{
		int length = readInt("Length");
		History h = new History();
		long total_time = 0;
		// Generate a sequence of given length
		for (int i = 0; i < length; i++)
		{
			try
			{
				m_manager.appendAction(h, "Alice", "a", "G1");
			} 
			catch (EncryptionException e) 
			{
				throw new ExperimentException(e);
			}
		}
		// Verify the sequence
		for (int num_reps = 0; num_reps < s_numRepetitions; num_reps++)
		{
			long start_time = System.currentTimeMillis();
			try 
			{
				m_manager.isHistoryValid(h);
			} 
			catch (InvalidHistoryException e) 
			{
				throw new ExperimentException(e);
			} 
			catch (EncryptionException e)
			{
				throw new ExperimentException(e);
			}
			long end_time = System.currentTimeMillis();
			total_time += (end_time - start_time);
		}
		// Since we are measuring milliseconds, we can afford
		// rounding to an integer
		int avg_time = (int) total_time / s_numRepetitions;
		write(TIME, avg_time);
	}
	
	@Override
	public float getDurationEstimate(float factor)
	{
		return readFloat(LENGTH) * 0.000125f / factor;
	}
}
